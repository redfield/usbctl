GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
PKGPATH ?= gitlab.com/redfield/usbctl
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/usbctl-service cmd/usbctl-service/*.go
	go build -o bin/usbctl-client cmd/usbctl-client/*.go
	go build -o bin/usbctl-auto-attach cmd/usbctl-auto-attach/*.go

.PHONY: install
install:
	install -d -m 0755 $(DESTDIR)/etc/dbus-1/system.d
	install -m 0644 configs/com.gitlab.redfield.api.usbctl.conf $(DESTDIR)/etc/dbus-1/system.d/

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ pkg/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/usbctl.pb.go: api/usbctl.proto
	protoc -I api --go_out=plugins=grpc:api api/usbctl.proto

.PHONY: proto
proto: api/usbctl.pb.go

.PHONY: check
check: goreportcard all test
	DESTDIR=/tmp make install

.PHONY: test
test:
	echo "We should probably have some tests :D"

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install
	go install `go list -f  "{{.ImportPath}}" "{{.TestGoFiles}}" ./...`

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll

