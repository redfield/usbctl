// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/redfield/usbctl/api"
	"google.golang.org/grpc"
)

// Command line flags
var (
	address  = flag.String("address", "localhost:50054", "Address of the usbctl service")
	domid    = flag.Int("domid", -1, "Domain ID (domid) of the domain to auto attach devices")
	class    = flag.Int("class", 0, "USB device class to auto attach to the domain")
	subclass = flag.Int("subclass", 0, "USB device subclass to auto attach to the domain")
)

func main() {
	flag.Parse()

	if *domid <= 0 {
		fmt.Println("Please specify a valid domid")
		os.Exit(1)
	}

	cc, err := grpc.Dial(*address, grpc.WithInsecure())
	if err != nil {
		fmt.Printf("Failed to create usbctl client: %v\n", err)
		os.Exit(1)
	}

	client := api.NewUsbCtlClientClient(cc)

	r := &api.AutoAttachByClassRequest{
		Domid:    strconv.Itoa(*domid),
		Class:    uint32(*class),
		Subclass: uint32(*subclass),
	}

	_, err = client.AutoAttachByClass(context.Background(), r)
	if err != nil {
		fmt.Printf("Failed to start auto-attaching: %v\n", err)
		os.Exit(1)
	}
}
