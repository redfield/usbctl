// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package usbctl

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/google/gousb"
	"github.com/google/gousb/usbid"
	pb "gitlab.com/redfield/usbctl/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

// Allow for policy based filtering of devices
func filterDevice(device *pb.UsbDevice) bool {
	// TODO: come up with filter format? some kinda json?

	// Filter out hub devices
	return !(device.Class == 9)
}

func usbDeviceList() []*pb.UsbDevice {
	var ret []*pb.UsbDevice

	ctx := gousb.NewContext()
	defer ctx.Close()

	// OpenDevices is used to find the devices to open.
	_, _ = ctx.OpenDevices(func(desc *gousb.DeviceDesc) bool {
		domid, _, _ := getDeviceAssigned(uint32(desc.Bus), uint32(desc.Address))
		state := 0
		if domid != 0 {
			state = 1
		}

		prettyName := usbid.Classify(desc)

		if prettyName == "(Defined at Interface level)" {
			for _, cfg := range desc.Configs {
				for _, intf := range cfg.Interfaces {
					for _, ifs := range intf.AltSettings {
						prettyName = usbid.Classify(ifs)
					}
				}
			}
		}

		prettyName = prettyName + " " + usbid.Describe(desc)

		usbDev := pb.UsbDevice{
			Name:          prettyName,
			State:         pb.UsbDevice_State(state),
			AssignedUuid:  "",
			Bus:           uint32(desc.Bus),
			Port:          uint32(desc.Port),
			Addr:          uint32(desc.Address),
			Speed:         uint32(desc.Speed),
			Spec:          uint32(desc.Spec),
			DeviceVersion: uint32(desc.Device),
			VendorId:      uint32(desc.Vendor),
			ProductId:     uint32(desc.Product),
			Class:         uint32(desc.Class),
			Subclass:      uint32(desc.SubClass),
			Protocol:      uint32(desc.Protocol),
		}

		if filterDevice(&usbDev) {
			ret = append(ret, &usbDev)
		}

		return false
	})

	return ret
}

func usbDeviceListByInterfaceClasses(class, subclass uint32) []*pb.UsbDevice {
	list := make([]*pb.UsbDevice, 0)

	ctx := gousb.NewContext()
	defer ctx.Close()

	_, _ = ctx.OpenDevices(func(desc *gousb.DeviceDesc) bool {
		match := false
		prettyName := usbid.Classify(desc)

	L:
		for _, cfg := range desc.Configs {
			for _, iface := range cfg.Interfaces {
				for _, s := range iface.AltSettings {
					if prettyName == "(Defined at Interface level)" {
						prettyName = usbid.Classify(s)
					}

					if uint32(s.Class) == class && uint32(s.SubClass) == subclass {
						match = true
						break L // Get me the heck outta here.
					}
				}
			}
		}

		if !match {
			return false
		}

		domid, _, _ := getDeviceAssigned(uint32(desc.Bus), uint32(desc.Address))
		state := 0
		if domid != 0 {
			state = 1
		}

		prettyName = prettyName + " " + usbid.Describe(desc)

		dev := pb.UsbDevice{
			Name:          prettyName,
			State:         pb.UsbDevice_State(state),
			AssignedUuid:  "",
			Bus:           uint32(desc.Bus),
			Port:          uint32(desc.Port),
			Addr:          uint32(desc.Address),
			Speed:         uint32(desc.Speed),
			Spec:          uint32(desc.Spec),
			DeviceVersion: uint32(desc.Device),
			VendorId:      uint32(desc.Vendor),
			ProductId:     uint32(desc.Product),
			Class:         uint32(desc.Class),
			Subclass:      uint32(desc.SubClass),
			Protocol:      uint32(desc.Protocol),
		}

		list = append(list, &dev)

		// Don't open any devices.
		return false
	})

	return list
}

type server struct{}

func runningDomids() []uint32 {
	xlListOut, err := exec.Command("xl", "list").Output()
	if err != nil {
		return []uint32{}
	}

	xlList := strings.Split(string(xlListOut), "\n")
	domidList := []uint32{}
	for _, line := range xlList {
		if !strings.Contains(line, "Name") &&
			!strings.Contains(line, "ID") &&
			!strings.Contains(line, "Mem") &&
			!strings.Contains(line, "VCPUs") &&
			!strings.Contains(line, "State") &&
			!strings.Contains(line, "Time(s)") {
			vmLine := strings.Fields(line)

			if len(vmLine) == 0 {
				continue
			}

			domid, err := strconv.Atoi(vmLine[1])
			if err != nil || domid < 0 {
				continue
			}

			domidList = append(domidList, uint32(domid))
		}
	}

	return domidList
}

func parseStartLine(line string) bool {
	return strings.Contains(line, "Devid  Type")
}

func parseControllerLine(line string) (int32, int32) {
	if !strings.Contains(line, "devicemodel") {
		return -1, -1
	}

	controllerFields := strings.Fields(line)

	controller, err := strconv.Atoi(controllerFields[0])
	if err != nil {
		fmt.Println("Failed to parse controller id")
		return -1, -1
	}

	portLimit, err := strconv.Atoi(controllerFields[5])
	if err != nil {
		fmt.Println("Failed to parse port limit")
		return -1, -1
	}

	// [devid, type, backend-domid, state, usb-ver, num-ports]
	return int32(controller), int32(portLimit)
}

func parsePortLine(line string) (int32, int32) {
	if !strings.Contains(line, "Port") {
		return -1, -1
	}

	portFields := strings.Fields(line)
	if len(portFields) == 2 || len(portFields) != 6 {
		// No device is hooked to this port if ==2 and invalid
		// data if != 6
		return -1, -1
	}

	hostBus, err := strconv.Atoi(portFields[3])
	if err != nil {
		return -1, -1
	}

	hostAddr, err := strconv.Atoi(portFields[5])
	if err != nil {
		return -1, -1
	}

	return int32(hostBus), int32(hostAddr)
}

const START = "START"
const CONTROLLER = "CONTROLLER"
const PORT = "PORT"

func parseUsbListOutput(output []string, hostBus uint32, hostAddr uint32) (uint32, uint32, error) {
	// Little parser to deal with xl usb-list output which
	// is wildly inconvenient in formatting
	parseState := START
	parseNextState := START
	var controller int32
	var parsedHostBus int32
	var parsedHostAddr int32
	var portLimit int32
	var portIter int32

	for _, line := range output {
		switch parseState {
		case START:
			if parseStartLine(line) {
				parseNextState = CONTROLLER
				controller = -1
				portLimit = -1
			}
		case CONTROLLER:
			controller, portLimit = parseControllerLine(line)
			if controller != -1 && portLimit != -1 {
				parseNextState = PORT
				parsedHostBus = -1
				parsedHostAddr = -1
				portIter = 0
			} else {
				parseNextState = START
			}
		case PORT:
			if portIter < portLimit {
				parseNextState = PORT
				parsedHostBus, parsedHostAddr = parsePortLine(line)
				portIter++
				if parsedHostBus == int32(hostBus) && parsedHostAddr == int32(hostAddr) {
					return uint32(controller), uint32(portIter), nil
				}
			}

			if portIter == portLimit {
				parseNextState = START
				portIter = 0
				controller = -1
				parsedHostAddr = -1
				parsedHostBus = -1
				portLimit = -1
			}
		}
		parseState = parseNextState
	}

	return 0, 0, fmt.Errorf("failed to find assigned device for %v:%v", hostBus, hostAddr)
}

func getDeviceAssigned(hostBus uint32, hostAddr uint32) (uint32, uint32, uint32) {
	domids := runningDomids()
	for _, domid := range domids {
		domString := fmt.Sprintf("%v", domid)
		usbListOut, err := exec.Command("xl", "usb-list", domString).Output()
		if err != nil {
			fmt.Println(err)
			continue
		}

		usbList := strings.Split(string(usbListOut), "\n")
		controller, port, err := parseUsbListOutput(usbList, hostBus, hostAddr)
		if err == nil {
			return domid, controller, port
		}
	}

	return 0, 0, 0
}

func (s *server) Attach(ctx context.Context, in *pb.AttachRequest) (*pb.AttachReply, error) {

	hostbus := fmt.Sprintf("hostbus=%v", in.GetDevice().GetBus())
	hostaddr := fmt.Sprintf("hostaddr=%v", in.GetDevice().GetAddr())
	var controllerIndex uint32

	switch in.GetDevice().GetSpeed() {
	case uint32(gousb.SpeedSuper):
		controllerIndex = 2
	case uint32(gousb.SpeedHigh):
		controllerIndex = 1
	case uint32(gousb.SpeedLow):
		controllerIndex = 0
	case uint32(gousb.SpeedFull):
		controllerIndex = 0
	}

	controller := fmt.Sprintf("controller=%v", controllerIndex)

	attach, err := exec.Command("xl", "usbdev-attach", in.GetDomid(), hostbus, hostaddr, controller).Output()
	if err != nil {
		return &pb.AttachReply{Reply: "Failed to attach device!"}, err
	}

	return &pb.AttachReply{Reply: string(attach)}, nil
}

func (s *server) Detach(ctx context.Context, in *pb.DetachRequest) (*pb.DetachReply, error) {
	domid, controller, port := getDeviceAssigned(in.GetDevice().GetBus(), in.GetDevice().GetAddr())

	if domid == 0 {
		return &pb.DetachReply{Reply: "Device is not currently assigned."}, nil
	}

	domidString := fmt.Sprintf("%v", domid)
	controllerString := fmt.Sprintf("%v", controller)
	portString := fmt.Sprintf("%v", port)
	out, err := exec.Command("xl", "usbdev-detach", domidString, controllerString, portString).Output()
	if err != nil {
		return &pb.DetachReply{Reply: "Failed to detach device for domain."}, nil
	}

	return &pb.DetachReply{Reply: string(out)}, nil
}

func (s *server) DeviceList(ctx context.Context, in *pb.DeviceListRequest) (*pb.DeviceListReply, error) {
	devices := usbDeviceList()
	deviceReply := pb.DeviceListReply{UsbDevices: devices}

	return &deviceReply, nil
}

func (s *server) AutoAttachByClass(ctx context.Context, in *pb.AutoAttachByClassRequest) (*pb.AutoAttachByClassReply, error) {
	class, subclass := in.GetClass(), in.GetSubclass()

	domid := in.GetDomid()
	if domid == "" {
		return nil, errors.New("must specifiy domid")
	}

	go s.attachByClassWhenDeviceAppears(domid, class, subclass)

	return &pb.AutoAttachByClassReply{}, nil
}

func (s *server) attachByClassWhenDeviceAppears(domid string, class, subclass uint32) {
	for {
		for _, dev := range usbDeviceListByInterfaceClasses(class, subclass) {
			if dev.State == pb.UsbDevice_ATTACHED {
				continue
			}

			log.Printf("Auto-attaching %s to domain %s", dev.Name, domid)

			r := &pb.AttachRequest{
				Domid:  domid,
				Device: dev,
			}

			_, err := s.Attach(context.Background(), r)
			if err != nil {
				log.Printf("Failed to auto-attach device: %v", err)
			}
		}

		time.Sleep(2 * time.Second)
	}
}

// StartUsbCtlService is the entry point for starting usb-server.
func StartUsbCtlService(lis net.Listener) {
	grpcServer := grpc.NewServer()
	srv := &server{}

	pb.RegisterUsbCtlClientServer(grpcServer, srv)
	go startDBusAdapter(srv)

	err := grpcServer.Serve(lis)
	if err != nil {
		fmt.Printf("Failed to serve grpc %v", err)
	}
}
